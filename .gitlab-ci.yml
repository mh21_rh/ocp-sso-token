---
include:
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-common.yml}
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-sast.yml}

yamllint:
  extends: .yamllint

tests:
  extends: .tox
  variables:
    DNF_REQUIREMENTS_EXTRAS: python$PYTHON
  parallel:
    matrix:
      - PYTHON: [3.10-devel, 3.11-devel, 3.12-devel, 3.13-devel]
        TOX_ARGS: [-epy, -espec]
      - PYTHON: ['3.8', '3.9']
        TOX_ARGS: [-espec]
  before_script:
    - export TOX_OVERRIDE=testenv.base_python=${PYTHON%-devel}

.install:
  before_script:
    - pip install --user .[release]

.tag-pipeline:
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never

dist:
  extends: [.cki_tools, .install]
  script:
    - python3 -m build
    - python3 -m twine check dist/*
  artifacts:
    paths:
      - dist/*

check-version:
  extends: [.cki_tools, .install, .tag-pipeline]
  stage: 💉
  script:
    - |
      # check tagged version and Python package version match
      python_version=$(python3 -c 'import ocp_sso_token; print(ocp_sso_token.__version__)')
      if [[ ${CI_COMMIT_TAG} != "v${python_version}" ]]; then
        echo "Git tag and Python version differ: ${CI_COMMIT_TAG} != v${python_version}"
        exit 1
      fi
      echo "VERSION=${python_version}" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env

pypi-release:
  extends: [.cki_tools, .install, .tag-pipeline]
  stage: 🚀
  script:
    - python3 -m twine upload dist/*
  dependencies: [dist]

gitlab-release:
  extends: .tag-pipeline
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: 🚀
  script:
    - echo "Creating release..."
  release:
    name: "Release $CI_COMMIT_TAG"
    description: $CI_COMMIT_TAG_MESSAGE
    tag_name: $CI_COMMIT_TAG
    assets:
      links:
        - name: PyPI
          url: https://pypi.org/project/ocp-sso-token/$VERSION
  dependencies: [check-version]
